# 使用CI產生Signed APK

### 1.設置keystore
將keystore放在專案根目錄底下

### 2.設定variables

##### 2-1 進入variables
進入Setting > CI/CD > variables
![image alt](./image/2018-06-18 15-46-14 的螢幕擷圖.png)

##### 2-2 新增variables
![image alt](./image/2018-06-18 15-46-25 的螢幕擷圖.png)

##### 2-3 在本地端專案根目錄新增一個檔案signing.properties，此檔案不加入Git
![image alt](./image/2018-06-18 16-09-24 的螢幕擷圖.png)

### 3.修改app build.gradle

```groovy
android {
    ...
    
    signingConfigs {
        sign
    }
    
    buildTypes {
        release {
            signingConfig signingConfigs.sign
            minifyEnabled true
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }
    
    ...
}

def keystore = rootProject.file('signing.properties')
if (keystore.canRead()) {
    def props = new Properties()
    props.load(new FileInputStream(keystore))
    if (props != null) {
        println 'signing'
        android.signingConfigs.sign.storeFile file(props['KEYSTORE_FILE'])
        android.signingConfigs.sign.storePassword props['KEYSTORE_PASSWORD']
        android.signingConfigs.sign.keyAlias props['KEYSTORE_ALIAS']
        android.signingConfigs.sign.keyPassword props['KEYSTORE_ALIAS_PASSWORD']
    } else {
        println 'some entries in \'signing.properties\' not found!'
    }
} else {
    println 'Use Gitlab CI Variables'
    android.signingConfigs.sign.storeFile file(System.getenv('KEYSTORE_FILE'))
    android.signingConfigs.sign.storePassword System.getenv('KEYSTORE_PASSWORD')
    android.signingConfigs.sign.keyAlias System.getenv('KEYSTORE_ALIAS')
    android.signingConfigs.sign.keyPassword System.getenv('KEYSTORE_ALIAS_PASSWORD')
}
```




